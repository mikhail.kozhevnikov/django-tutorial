# Portfolio project based on Python Django Tutorial.

## Goals:

## TODO:
 - [ ] Create pet project
 - [X] Create Dockerfile
 - [ ] Build Docker image with gitlab ci
 - [ ] Add Tests
 - [ ] Describe infrastructure as a code for project
 - [ ] Store all secrets in Vault
 - [ ] Create CI/CD pipelines
 - [ ] Write docs
 - [ ] ...
 - [ ] Profit

## How to setup